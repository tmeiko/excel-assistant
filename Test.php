<?php


class Test
{

    public function TestExport()
    {
        include 'src/ExportExcel.php';
        $array = [
            [
              'a'=>1,
              'b'=>2,
              'c'=>3
            ],
            [
                'a'=>99,
                'b'=>88,
                'c'=>77
            ],
        ];
        $header = [
            '姓名'=>'a',
            '姓别'=>'b',
            '年龄'=>'c',
        ];
        $filename ='测试导出_'.time();
        \Excel\ExcelHelper\ExportExcel::export($array,array_keys($header),$filename);
    }

    public function TestImport()
    {
        include 'src/ImportExcel.php';
        $file_path = './测试导出_1712119976.xls';
        $import = new \Excel\ExcelHelper\ImportExcel($file_path);
        $res = $import->import();
        var_dump($res);
        die();
    }

}

//导出测试及用法
$a = new Test();
$a->TestImport();