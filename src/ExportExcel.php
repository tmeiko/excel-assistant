<?php

namespace Excel\ExcelHelper;

class ExportExcel
{
    public static function export(array $data, array $headers , string $filename)
    {
        require_once  'Classes/PHPExcel.php';
        $objPHPExcel = new \PHPExcel();
        // 表头
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $c = 0;
        foreach ($headers as $header) {
            $sheet->setCellValue(self::stringFromColumnIndex($c) . '1', $header);
            $c++;
        }
        $col_num = 2;
        foreach ($data as $item) {
            $c = 0;
            foreach ($item as $v) {
                $sheet->setCellValue(self::stringFromColumnIndex($c) . $col_num, (is_numeric($v) && (strlen($v) >12)) ? ' '.$v : $v);
                $c++;
            }
            $col_num++;
        }
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=1');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

    }

    public static function stringFromColumnIndex($pColumnIndex = 0)
    {
        static $_indexCache = array();

        if (!isset($_indexCache[$pColumnIndex])) {
            if ($pColumnIndex < 26) {
                $_indexCache[$pColumnIndex] = chr(65 + $pColumnIndex);
            } elseif ($pColumnIndex < 702) {
                $_indexCache[$pColumnIndex] = chr(64 + ($pColumnIndex / 26)) . chr(65 + $pColumnIndex % 26);
            } else {
                $_indexCache[$pColumnIndex] = chr(64 + (($pColumnIndex - 26) / 676)) . chr(65 + ((($pColumnIndex - 26) % 676) / 26)) . chr(65 + $pColumnIndex % 26);
            }
        }

        return $_indexCache[$pColumnIndex];
    }


}