<?php

namespace Excel\ExcelHelper;
require_once  'Classes/PHPExcel.php';
class ImportExcel
{

    protected $filePath;
    protected $reader;

    protected $spreadsheet;

    protected $sheet;



    public function __construct($filePath) {
        $this->filePath = $filePath;
        $this->reader = \PHPExcel_IOFactory::createReaderForFile($this->filePath);
        $this->spreadsheet = new \PHPExcel();
    }

    /**
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function import()
    {
        $this->reader->setReadDataOnly(true);
        $excel =$this->reader->load($this->filePath);
        // 获取工作表
        $sheet = $excel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $batchSize = 100; //每批处理100条数据
        $currentRow = 2; //从第2行开始
        $return = [];
        while ($currentRow <= $highestRow) {
            // 读取一行数据
            $rowData = $sheet->rangeToArray('A' . $currentRow . ':' . $highestColumn . $currentRow, NULL, TRUE, FALSE);
            // 处理这行数据
            $return[]=$rowData;
            $currentRow++;
            if ($currentRow % $batchSize == 2) {
                //每处理100条数据，就清空内存
                $this->spreadsheet->disconnectWorksheets();
                unset($this->spreadsheet);
                $this->reader = \PHPExcel_IOFactory::createReaderForFile($this->filePath);
                $this->spreadsheet = new \PHPExcel();
            }
        }

        return $return;
    }





}